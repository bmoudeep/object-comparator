import java.lang.reflect.*;

class COMPARE
 {
  public static boolean equals(Object obj1, Object obj2) 
    {   boolean chk = true;
	    // check for null object and for checking if they belong to same class or not
        if((obj1 == null || obj2 == null)|| obj1.getClass()!= obj2.getClass()) 
            return  false; 
		// check if they share same memory	
		if(obj1 == obj2) 
            return true; 
		// using reflection to the the values	
		Class cls = obj1.getClass(); 
		Field[] fld = cls.getDeclaredFields();
		//Exception handling
		try{
		for(Field f: fld)
		 {
          f.setAccessible(true);
		  if(f.get(obj1) == f.get(obj2))
           	 chk = true;
          else
		   {
		    chk = false;
            break;		
           }
          }			
		 }catch(IllegalAccessException ex){System.out.println("ERROR");} 
		return chk; 
    }  
  }	
	
class Teen_Titans
 {
  private String Name;
  private int rating;
  Teen_Titans(String s, int i)
   {
    Name = s;
	rating = i;
   }
 }    

class JUSTICE_LEAGUE
 {
  private String Name;
  private int rank;
  private int rating;
  JUSTICE_LEAGUE(String s, int r, int i)
   {
    Name = s;
	rank = r;
	rating = i;
   }
 }    

class Launcher
 {
  public static void main(String[] args)
   {
    Teen_Titans t = new Teen_Titans("Robin", 9);
    JUSTICE_LEAGUE j = new JUSTICE_LEAGUE("Batman", 1, 9);
    Teen_Titans tt = new Teen_Titans("Raven", 9);
    JUSTICE_LEAGUE jj = new JUSTICE_LEAGUE("Batman", 1, 9);
    boolean chk;
    chk = COMPARE.equals(t,j);
    if(chk)
     System.out.println("EQUAL");
    else
     System.out.println("NOT EQUAL");  
    chk = COMPARE.equals(t,tt);
    if(chk)
     System.out.println("EQUAL");
    else
     System.out.println("NOT EQUAL");  
    chk = COMPARE.equals(j,jj);
    if(chk)
     System.out.println("EQUAL");
    else
     System.out.println("NOT EQUAL"); 
    chk = COMPARE.equals(t,t);
    if(chk)
     System.out.println("EQUAL");
    else
     System.out.println("NOT EQUAL"); 
    chk = COMPARE.equals(j,j);
    if(chk)
     System.out.println("EQUAL");
    else
     System.out.println("NOT EQUAL"); 
	Teen_Titans t1 = t; 
    JUSTICE_LEAGUE j1 = j;
    chk = COMPARE.equals(t,t1);
    if(chk)
     System.out.println("EQUAL");
    else
     System.out.println("NOT EQUAL"); 
    chk = COMPARE.equals(j,j1);
    if(chk)
     System.out.println("EQUAL");
    else
     System.out.println("NOT EQUAL"); 	 
   }
}
   